
#ifndef WHEEL_H
#define WHEEL_H


#include <QObject>


class Wheel : public QObject
{
    Q_OBJECT


private:


public:
    explicit Wheel(QObject *parent = nullptr);


    void emitWheelCreated();


signals:
    void wheelCreated();
};

#endif // WHEEL_H
