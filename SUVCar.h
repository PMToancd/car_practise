
#ifndef SUVCAR_H
#define SUVCAR_H
#include <QVector>
#include "Wheel.h"
#include <QThread>
#include <QString>
#include <QDebug>

class SUVCar : public QThread
{
    Q_OBJECT


private:
    QString name;

    Wheel w;
    Wheel* w1;
    int count;
public:

    explicit SUVCar();

    void startUp();

    void setName(QString name);

    QString getName();

    void receiveWheelCreated();
    void emitWheelCreated();

    void run();
signals:
    void carCreated(QString name);
};

#endif // SUVCAR_H
