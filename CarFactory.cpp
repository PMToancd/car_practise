
#include "CarFactory.h"
#include "SUVCar.h"
CarFactory::CarFactory(QObject *parent)
    : QObject{parent}
{

}

void CarFactory::callStartUp()
{
    //    s.startUp();
}

void CarFactory::callSetName()
{

    s1->setName("Mec");
}

QString CarFactory::callGetName()
{
    return s1->getName();
}

void CarFactory::display()
{
    callSetName();
    qDebug() << callGetName();
}

void CarFactory::initCars()
{
//    for(int i = 0; i < 10; i++) {
//        SUVCar* s1 = new SUVCar();
//        emit s1->carCreated(QString::number(i));
//    };

    for(int i = 0; i < 10; i++) {
        SUVCar* s1 = new SUVCar();
        QObject::connect(s1, &SUVCar::carCreated, this, &CarFactory::receiveCarCreated);
        s1->setName(QString::number(i)); //su dung con tro s1
        s1->start();
        // SUVCar sẽ tạo một thread 0 (i=0) và bên trong mỗi cái thread mới tạo nó sẽ thực hien công việc của nó
        //(thực hiện ham run() của nó), chương trình chính sẽ tiếp tục thực hiện tác vụ của nó là tạo tiếp một thread 1, 2, 3, 4,...
/*tóm lai khi ta chạy loop sẽ liên tục tạo ra thread và ở bên trong mỗi thread sẽ tự thực hiện tác vụ của nó chứ chương trình chính
  sẽ tiếp tục thực hiện chứ không đợi cái bên trong thread thực hiện xong mới làm => các threads sẽ lam viec song song với nhau */
//        s1->startUp(i);
        _cars.push_back(s1);

//        emit s.carCreated(QString::number(i)); //su dung doi tuong s
//        delete s1;
    }
}

void CarFactory::callCars()
{
    for(int i = 0; i < _cars.size(); i++) {
        qDebug() << _cars[i]->getName();
    };
}

void CarFactory::callWheelsEnough()
{
    s.emitWheelCreated();
}

void CarFactory::receiveCarCreated(QString name)
{
    qDebug() << "Car " << name << " signal received";
}
