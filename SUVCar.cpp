
#include "SUVCar.h"

SUVCar::SUVCar()
{
//    QObject::connect(&w, &Wheel::wheelCreated, this, &SUVCar::receiveWheelCreated);
}

void SUVCar::startUp()
{
    qDebug() << "Thread" << getName();
    emit carCreated(getName());

//    emit carCreated(QString::number(threadCount));

}

void SUVCar::setName(QString name)
{

    this->name = name;
}

QString SUVCar::getName()
{
    return name;
}

void SUVCar::receiveWheelCreated()
{
    qDebug() << "A wheel created";
    count++;

    if(count == 4) {
        qDebug() << "Wheels Enough";
    }
}

void SUVCar::emitWheelCreated()
{

    count = 0;
    for(int i = 0; i < 4; i++) {
       // emit w.wheelCreated(); //sử dụng đối tượng w

        Wheel* w1 = new Wheel();
        QObject::connect(w1, &Wheel::wheelCreated, this, &SUVCar::receiveWheelCreated);
//        // vi line 42 mới định nghĩa con trỏ w1 chứ Wheel* w1 vẫn chưa đn w1 nên trình biên dịch sẽ khong hieu
        emit w1->wheelCreated(); // sử dụng đối tượng con trỏ w1
    }
}

void SUVCar::run()
{
    while (true) {
        startUp();
        msleep((name.toInt()+1) * 1000);
    }
}



