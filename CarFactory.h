
#ifndef CARFACTORY_H
#define CARFACTORY_H

#include "SUVCar.h"
#include <QObject>
#include <QString>
#include <QDebug>
#include <QVector>

class CarFactory : public QObject
{
    Q_OBJECT
public:



private:
    SUVCar s;
    SUVCar* s1;
    QVector<SUVCar*> _cars;
public:

    explicit CarFactory(QObject *parent = nullptr);

    void callStartUp();

    void callSetName();

    QString callGetName();

    void display();

    void initCars(); //emit

    void callCars();

    void callWheelsEnough();

public slots:
    void receiveCarCreated(QString name);


};

#endif // CARFACTORY_H
